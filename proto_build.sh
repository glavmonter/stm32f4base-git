#!/bin/bash

NANOPB_GENERATOR="/home/margel/bin/nanopb/generator"

PROTOS="VGFThermo_nano.proto"
PROTOS_PATH="."

for f in $PROTOS
do
    echo "Processing $f"
    base=$(basename $f)
    filename=$(echo ${base%.*} | sed 's/_nano//g')
    
    protoc -I. -I /usr/include/ -I$PROTOS_PATH -I $NANOPB_GENERATOR/proto -o $filename.pb $f
    python $NANOPB_GENERATOR/nanopb_generator.py $filename.pb
    mv $filename.pb.h include
    mv $filename.pb.c src
    rm $filename.pb    
done

